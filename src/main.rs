#![no_main]
#![no_std]
#![feature(alloc)]
#![feature(alloc_error_handler)]

// #[macro_use]
// extern crate cortex_m_rt as rt;
// extern crate cortex_m;
// extern crate panic_semihosting;
// extern crate stm32f103xx_hal as hal;
extern crate alloc;
extern crate alloc_cortex_m;

use stm32f103xx_hal as hal;
use embedded_hal::{PwmPin};
use cortex_m_rt as rt;
use cortex_m::asm;
use self::hal::prelude::*;
use self::hal::{stm32f103xx};
// use self::rt::ExceptionFrame;
// use self::rt::{entry, exception};
use self::rt::{entry};
use self::alloc::boxed::Box;
use alloc_cortex_m::CortexMHeap;

mod rgb_led;

#[global_allocator]
static ALLOCATOR: CortexMHeap = CortexMHeap::empty();

#[allow(unused_imports)]
use panic_semihosting;

const HEAP_SIZE: usize = 1024; // in bytes

#[entry]
fn main() -> ! {
    unsafe { ALLOCATOR.init(cortex_m_rt::heap_start() as usize, HEAP_SIZE) }

    let p = stm32f103xx::Peripherals::take().unwrap();

    let mut flash = p.FLASH.constrain();
    let mut rcc = p.RCC.constrain();

    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let delay = hal::delay::Delay::new(cortex_m::Peripherals::take().unwrap().SYST, clocks);

    let mut afio = p.AFIO.constrain(&mut rcc.apb2);

    let mut gpioa = p.GPIOA.split(&mut rcc.apb2);
    let mut gpiob = p.GPIOB.split(&mut rcc.apb2);

    let t2cs = p.TIM2
        .pwm(
            (
                gpioa.pa0.into_alternate_push_pull(&mut gpioa.crl),
                gpioa.pa1.into_alternate_push_pull(&mut gpioa.crl),
                gpiob.pb10.into_alternate_push_pull(&mut gpiob.crh),
                gpiob.pb11.into_alternate_push_pull(&mut gpiob.crh)
            ),
            &mut afio.mapr,
            10.khz(),
            clocks,
            &mut rcc.apb1,
        );

    let t3cs = p.TIM3
        .pwm(
            (
                gpioa.pa6.into_alternate_push_pull(&mut gpioa.crl),
                gpioa.pa7.into_alternate_push_pull(&mut gpioa.crl),
                gpiob.pb0.into_alternate_push_pull(&mut gpiob.crl),
                gpiob.pb1.into_alternate_push_pull(&mut gpiob.crl)
            ),
            &mut afio.mapr,
            10.khz(),
            clocks,
            &mut rcc.apb1,
        );

    let t4cs = p.TIM4
        .pwm(
            (
                gpiob.pb6.into_alternate_push_pull(&mut gpiob.crl),
                gpiob.pb7.into_alternate_push_pull(&mut gpiob.crl),
                gpiob.pb8.into_alternate_push_pull(&mut gpiob.crh),
                gpiob.pb9.into_alternate_push_pull(&mut gpiob.crh)
            ),
            &mut afio.mapr,
            10.khz(),
            clocks,
            &mut rcc.apb1,
        );

    // let mut pwm = t2cs.0;
    // let max = pwm.get_max_duty();

    // pwm.enable();
       
    // pwm.set_duty(max);



    let mut led1 = rgb_led::LED::new(t2cs.0, t3cs.0, t4cs.0);
    led1.enable();
    led1.set_colour(0xFFD000_u32);

    loop {}
}

#[alloc_error_handler]
fn foo(_: core::alloc::Layout) -> ! {
    panic!("alloc");
}
