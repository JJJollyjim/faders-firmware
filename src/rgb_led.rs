extern crate alloc;

use embedded_hal::{PwmPin};
use self::alloc::boxed::Box;

pub struct LED<R: PwmPin<Duty = u16>, G: PwmPin<Duty = u16>, B: PwmPin<Duty = u16>> {
    r: R,
    g: G,
    b: B,
    rmd: u16,
    gmd: u16,
    bmd: u16,
}

impl<R,G,B> LED<R,G,B>
where
    R: PwmPin<Duty = u16>,
    G: PwmPin<Duty = u16>,
    B: PwmPin<Duty = u16>
{
    pub fn new(r: R, g: G, b: B) -> LED<R,G,B> {
        LED {
            rmd: 800, // TODO
            gmd: 800,
            bmd: 800,
            r: r,
            g: g,
            b: b,
        }
    }

    pub fn enable(&mut self) {
        self.r.enable();
        self.g.enable();
        self.b.enable();
    }

    pub fn disable(&mut self) {
        self.r.disable();
        self.g.disable();
        self.b.disable();
    }

    pub fn set_colour(&mut self, colour: u32) {
        let red   = (colour >> 16) & 0xFF;
        let green = (colour >> 8)  & 0xFF;
        let blue  = (colour >> 0)  & 0xFF;

        // TODO gamma correction

        let rd = ((red   * (self.rmd as u32)) / 0xFF) as u16;
        let gd = ((green * (self.gmd as u32)) / 0xFF) as u16;
        let bd = ((blue  * (self.bmd as u32)) / 0xFF) as u16;

        self.r.set_duty(rd);
        self.g.set_duty(gd);
        self.b.set_duty(bd);

    }
}
