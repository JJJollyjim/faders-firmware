# Fader board firmware 

## Hardware

This firmware is written for the STM32F103xx line of processors, and specfically is run on [this](https://wiki.stm32duino.com/index.php?title=Blue_Pill) common generic STM32 board.

## Dependencies

To build this firmware you'll need:

- A recent rust nightly

- `rust-std` components (pre-compiled `core` crate) for the ARM Cortex-M
  target. Run:

``` console
$ rustup target add thumbv7m-none-eabi
```

# License

```
    Fader board firmware
    Copyright (C) 2018 Jamie McClymont <jamie@kwiius.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
